package gr.soikon.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import gr.soikon.domain.Properties;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

public class JsonReader {

    private Properties properties = new Properties();

    private static final String PROPERTY_FILE_URL = "properties.json";

    private static final Path   currentRelativePath = Paths.get("");
    private static final String LOCALWORKINGDIR     = currentRelativePath.toAbsolutePath().toString() + "/" +
                                                      PROPERTY_FILE_URL;
    FileInputStream                   fileInputStream   = new FileInputStream(new File(LOCALWORKINGDIR));
    InputStreamReader                 inputStreamReader = new InputStreamReader(fileInputStream, "UTF-8");
    com.google.gson.stream.JsonReader reader            = new com.google.gson.stream.JsonReader(inputStreamReader);

    private void populatePropertyMap() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

        properties = gson.fromJson(reader, Properties.class);
    }

    public JsonReader() throws UnsupportedEncodingException, FileNotFoundException {
        populatePropertyMap();
    }

    public Properties getProperties() {
        return properties;
    }
}