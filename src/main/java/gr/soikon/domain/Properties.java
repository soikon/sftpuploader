package gr.soikon.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
public class Properties implements Serializable {

    @Expose
    @SerializedName("accounts")
    private List<Account> accountList;

    @Expose
    @SerializedName("exec")
    private Map<String, String> shellCommands;
}
