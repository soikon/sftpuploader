package gr.soikon.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.io.Serializable;

@Data
public class Account implements Serializable {

    @Expose
    @SerializedName("loginUsername")
    private String username;

    @Expose
    @SerializedName("loginPassword")
    private String password;
}