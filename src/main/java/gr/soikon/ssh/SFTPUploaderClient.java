package gr.soikon.ssh;

import com.jcraft.jsch.*;
import gr.soikon.domain.Properties;
import gr.soikon.utils.JsonReader;

import java.io.*;
import java.util.Arrays;

public class SFTPUploaderClient {

    public static void main(String options[]) throws UnsupportedEncodingException, FileNotFoundException {

        //Read properties from JSON file
        JsonReader jsonReader = new JsonReader();
        Properties properties = jsonReader.getProperties();

        boolean isSftp = (Arrays.asList("upload", "download").contains(options[0].toLowerCase()));

        String action   = options[0];
        String host     = options[1];
        String username = options[2];
        String password = options[3];
        String path     = "";
        String dest     = "";

        if (options.length > 4) {
            path = options[4];
            dest = options[5];
        }

//        Arrays.stream(options).forEach(option -> System.out.println(option));

        JSch    jsch = new JSch();
        Session session;
        try {
            session = jsch.getSession(username, host, 22);
            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword(password);
            session.connect();

            if (isSftp) {

                Channel channel = session.openChannel("sftp");
                channel.connect();
                ChannelSftp sftpChannel = (ChannelSftp) channel;
                if ("upload".equalsIgnoreCase(action)) {
                    sftpChannel.put(path, dest);
                } else if ("download".equalsIgnoreCase(action)) {
                    sftpChannel.get(path, dest);
                }
                sftpChannel.exit();
            } else {
                Channel        channel     = session.openChannel("exec");
                ChannelExec    execChannel = (ChannelExec) channel;
                BufferedReader in          = new BufferedReader(new InputStreamReader(channel.getInputStream()));
                if (!properties.getShellCommands().containsKey(options[0])) {
                    System.out.println("Invalid remote shell command.");
                    System.exit(0);
                }
                execChannel.setCommand(properties.getShellCommands().get(options[0]));

                execChannel.connect();

                String msg;
                while ((msg = in.readLine()) != null) {
                    System.out.println(msg);
                }

                channel.disconnect();
            }
            session.disconnect();
        } catch (JSchException | SftpException | IOException e) {
            e.printStackTrace();
        }
    }
}
